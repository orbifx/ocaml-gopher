let ending = "\n."

let of_string menu =
  let lines = String.split_on_char '\n' menu in
  List.map Item.of_line lines
